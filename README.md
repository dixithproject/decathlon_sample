### How do I get set up? ###

* Install Node & npm in your local machine
* Clone repository
* Install Dependencies - If npm is 6x then ``npm i``, if 7x then ``npm install -g npm`` & ``npm i --legacy-peer-deps``
* Start Application - ``npm start``

Runs the app in the development mode.<br>
Open [http://localhost:3000/decathlon] to view it in the browser.

### Login Credentials ? ###
username: admin
password: admin
