import { MinusOutlined, PlusOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { Button, Col, Divider, Input, List, message, Modal, Result, Row, Tag } from "antd";
import { cloneDeep, findIndex, get, toNumber } from "lodash";
import React from "react";
import { getSumArrayOfObjWithKey, getScreenSize, getToken, numberWithCommas } from "../../helper";
import { clearCookie, getCookie } from "../../helper/cookieHelper";
import "./index.scss";

/**
 * Checkout Component
 * @param props
 */
const Checkout = props => {
  const { match, history } = props;

  const [cartItems, setCartItems] = React.useState([]);

  React.useEffect(() => {
    if (!getToken()) {
      history.push(`/${get(match, "params.unique_name")}/login`);
    }
  }, []);

  React.useEffect(() => {
    const cartItemsList = getCookie("cart_items") ? getCookie("cart_items") : [];
    setCartItems(cartItemsList);
  }, []);

  const handleCartItem = (type, id, value) => {
    let cartItemsCopy = cloneDeep(cartItems);
    const getIndexObjectById = findIndex(cartItems, function (o) {
      return id === get(o, "id");
    });
    let currentObj = cartItemsCopy[getIndexObjectById];
    let currentQty = toNumber(get(currentObj, "quantity", 1));

    if (type === "add") {
      currentObj["quantity"] = currentQty + 1;
    }

    if (type === "remove" && currentQty) {
      currentObj["quantity"] = currentQty - 1;
    }

    if (type === "input" && Number(value) > -1) {
      currentObj["quantity"] = value;
    }

    currentObj["calculated_price"] = get(currentObj, "quantity") * get(currentObj, "mrp_price");

    cartItemsCopy[getIndexObjectById] = currentObj;
    setCartItems(cartItemsCopy);
  };

  const calculateTotalProducts = () => {
    const totalPrice = getSumArrayOfObjWithKey(cartItems, "calculated_price");
    return totalPrice;
  };

  const calculateTotalValue = () => {
    return calculateTotalProducts() ? calculateTotalProducts() + 199 : 0;
  };

  const handleCheckOut = () => {
    Modal.success({
      maskClosable: false,
      className: "order_success",
      centered: true,
      icon: <></>,
      content: (
        <>
          <Result
            status="success"
            title="Order Successfully Placed"
            subTitle="Order number: 2017182818828182881"
            extra={[
              <Button
                type="primary"
                key="shopping"
                onClick={() => {
                  clearCookie("cart_items");
                  window.location.href = `/${get(match, "params.unique_name")}`;
                }}
              >
                Continue Shopping
              </Button>
            ]}
          />
        </>
      )
    });
  };

  return (
    <div className="app__cart">
      <Row gutter={[16, 16]}>
        <Col xs={24} lg={12} sm={24} md={12}>
          <List
            itemLayout="horizontal"
            dataSource={cartItems}
            renderItem={item => (
              <List.Item
                extra={
                  <img
                    alt="product_image"
                    className="mt-3"
                    style={getScreenSize() > 480 ? { width: 270 } : { width: 280 }}
                    src={get(item, "image_src")}
                  />
                }
              >
                <List.Item.Meta
                  title={get(item, "item_name")}
                  description={
                    <>
                      <div>
                        <Tag color="#ffea29" className="text-dark" style={{ fontWeight: 500 }}>
                          ₹ {get(item, "item_price")}
                        </Tag>
                        <span className="pl-3">MRP ₹ {get(item, "mrp_price")}</span>
                      </div>
                      <div className="d-flex align-content-between mt-3">
                        <Button icon={<MinusOutlined />} onClick={() => handleCartItem("remove", get(item, "id"))} />
                        <Input
                          style={{ width: 100 }}
                          value={get(item, "quantity", 1)}
                          onChange={e => handleCartItem("input", get(item, "id"), e.target.value)}
                        />
                        <Button icon={<PlusOutlined />} onClick={() => handleCartItem("add", get(item, "id"))} />
                      </div>
                    </>
                  }
                />
              </List.Item>
            )}
          />
        </Col>
        <Col xs={24} lg={{ span: 8, offset: 4 }} sm={{ span: 8, offset: 4 }} md={{ span: 8, offset: 4 }}>
          <h5>PRICE DETAILS</h5>
          <Divider />
          <div className="d-flex justify-content-between">
            <span> Total products (Inc GST) </span>
            <span> ₹ {numberWithCommas(calculateTotalProducts())} </span>
          </div>
          <div className="d-flex justify-content-between mt-2">
            <span> Estimated Delivery Fee </span>
            <span> ₹ {calculateTotalProducts() ? 199 : 0} </span>
          </div>
          <Divider />
          <div className="d-flex justify-content-between">
            <b> Total </b>
            <b> ₹ {numberWithCommas(calculateTotalValue())} </b>
          </div>
          <div className="add_to_cart_button">
            <Button
              type="primary"
              icon={<ShoppingCartOutlined />}
              onClick={() => {
                if (calculateTotalValue()) {
                  clearCookie("cart_items");
                  handleCheckOut();
                } else {
                  message.info("Please add quantity");
                }
              }}
              className="mt-3 float-right"
            >
              PROCEED TO CHECKOUT
            </Button>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Checkout;
