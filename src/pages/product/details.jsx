import { ShoppingCartOutlined } from "@ant-design/icons";
import { Breadcrumb, Button, Col, Row, Tag } from "antd";
import { find, get } from "lodash";
import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { productList } from "../../constants";
import CartActions from "../../store/cart/actions";
import "./index.scss";
const { addToCart } = CartActions;

/**
 * Product Component
 * @param props
 */
const ProductDetails = props => {
  const { match, history } = props;
  const dispatch = useDispatch();

  const availableProduct = find(productList, function (o) {
    return get(o, "id") == get(match, "params.product_id");
  });

  const handleAddToCart = () => {
    dispatch(addToCart(availableProduct));
    history.push(`/${get(match, "params.unique_name")}/checkout`);
  };

  return (
    <div className="product_page">
      <div className="product_details_page__category_list mt-3">
        <Row gutter={[16, 16]}>
          <Col xs={24} lg={10} sm={24} md={10}>
            <Breadcrumb>
              <Breadcrumb.Item>
                <Link to={`/${get(match, "params.unique_name")}`}> All Sports </Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Category Name</Breadcrumb.Item>
              <Breadcrumb.Item>{get(availableProduct, "item_name")}</Breadcrumb.Item>
            </Breadcrumb>
            <img src={get(availableProduct, "image_src")} alt="product_image" className="w-100" />
          </Col>
          <Col xs={24} lg={14} sm={24} md={14}>
            <div className="product_details_page__description">
              <div className="product_details_page__description__product_name">
                <small>FORCLAZ</small>
                <h1>{get(availableProduct, "item_name")}</h1>
                <p>Reference: 8370601</p>
              </div>
              <div className="product_details_page__description__product_price">
                <Tag color="#ffea29" className="text-dark price_tag" style={{ fontWeight: 500 }}>
                  ₹ {get(availableProduct, "item_price")}
                </Tag>
                <span className="pl-3 mrp_price">MRP ₹ {get(availableProduct, "mrp_price")}</span>
              </div>

              <div className="mt-3">
                <b>A score for comparing products' environmental impact</b>
                <p className="mt-2">
                  The environmental impact of the product is calculated based on its entire life cycle and using different indicators. An overall
                  ABCDE score is given to help you easily identify products with the best environmental performance by comparing products of the same
                  type (T-shirts, trousers, backpacks, etc.). Decathlon is a voluntary supporter of this environmentally-friendly labelling approach.
                  For more information: http://sustainability.decathlon.com/
                </p>
                <b className="mt-2">MRP</b>
                <p className="mt-2"> ₹ 1,699 inclusive of all taxes</p>
              </div>

              <div className="mt-3 add_to_cart_button">
                <Button type="primary" icon={<ShoppingCartOutlined />} onClick={() => handleAddToCart()}>
                  ADD TO CART
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default ProductDetails;
