import { Card, Tag, Rate, Carousel, Col, Row } from "antd";
import React from "react";
import "./index.scss";
import { Link } from "react-router-dom";
const { Meta } = Card;
import { productList } from "../../constants";
import { get } from "lodash";

/**
 * Home Component
 * @param props
 */
const Home = props => {
  const { match } = props;
  return (
    <div className="app__home">
      <div className="app__home__carousel">
        <Carousel autoplay>
          <div>
            <img
              alt="Wind proof jacket"
              style={{ width: "100%" }}
              src="https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fs898657%2Fk%242667c6c4ac241e396e97a2c01f2268de%2F1.jpg%3Ff%3D1920x500%26format%3Dauto&w=1920&q=75"
            />
          </div>
          <div>
            <img
              alt="Sweatshirts"
              style={{ width: "100%" }}
              src="https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fs896842%2Fk%240a5898770a25c054654373fa46702c33%2Fsweat%252047.jpg%3Ff%3D1920x500%26format%3Dauto&w=1920&q=75"
            />
          </div>
          <div>
            <img
              alt="Running Apparel"
              style={{ width: "100%" }}
              src="https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fs896849%2Fk%248116bfe97ecddfe45c42d5de3922e991%2Frunning%252047.jpg%3Ff%3D1920x500%26format%3Dauto&w=1920&q=75"
            />
          </div>
          <div>
            <img
              alt="Decathlon Beanies"
              style={{ width: "100%" }}
              src="https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fs896862%2Fk%248750a28c51ab65f58dcc1d923cfd7b05%2Fbeanies%252047.jpg%3Ff%3D1920x500%26format%3Dauto&w=1920&q=75"
            />
          </div>
        </Carousel>
      </div>
      <div className="app__home__product_list mt-5">
        <h2>
          <span>TRENDING </span>
          <p>NEAR YOU</p>
        </h2>
        <Row gutter={[16, 16]}>
          {productList.map((list, index) => {
            return (
              <Col xs={24} lg={6} sm={12} md={6} key={index}>
                <Link to={`/${get(match, "params.unique_name")}/product/${get(list, "id")}/details`}>
                  <Card
                    className="p-3"
                    hoverable
                    cover={
                      <>
                        <img alt="example" src={get(list, "image_src")} />
                        <Rate disabled allowHalf value={get(list, "rating")} style={{ textAlign: "right" }} />
                      </>
                    }
                  >
                    <Meta
                      title={get(list, "item_name")}
                      description={
                        <div>
                          <Tag color="#ffea29" className="text-dark" style={{ fontWeight: 500 }}>
                            ₹ {get(list, "item_price")}
                          </Tag>
                          <span className="pl-3">MRP ₹ {get(list, "mrp_price")}</span>
                        </div>
                      }
                    />
                  </Card>
                </Link>
              </Col>
            );
          })}
        </Row>
      </div>
    </div>
  );
};

export default Home;
