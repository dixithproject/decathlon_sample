import { Button, Col, Form, Input, message, Row } from "antd";
import { get } from "lodash";
import React from "react";
import { setToken } from "../../helper";
import "./index.scss";

/**
 * Login Component
 * @param props
 */
const Login = props => {
  const { match, history } = props;
  const [username, setUserName] = React.useState("");
  const [password, setPassword] = React.useState("");

  const handleSubmit = () => {
    if (username === "admin" && password === "admin") {
      setToken("decathlon_test_sample_56dd0022-5f57-11ec-bf63-0242ac130002");
      message.success("Login Successful");
      history.goBack();
      // history.push(`/${get(match, "params.unique_name")}`);
    } else {
      message.error("Invalid Credentials");
    }
  };

  return (
    <div className="app__login">
      <Row gutter={[16, 16]}>
        <Col xs={24} lg={12} sm={24} md={12}>
          <div className="app__login_content">
            <h4 className="text-uppercase">Login </h4>
            <div>
              <h3>Get </h3>
              <h3>access to</h3>
              <h3 style={{ color: "#0082c3" }}>personalised</h3>
              <h3>shopping experience</h3>
              <div className="text-right">
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMTIuNjg1IiBoZWlnaHQ9IjEzNy40NzUiIHZpZXdCb3g9IjAgMCAxMTIuNjg1IDEzNy40NzUiPgogIDxnIGlkPSJHcm91cF8zMDMiIGRhdGEtbmFtZT0iR3JvdXAgMzAzIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTE0My4zOTUgLTIyNy41MjUpIj4KICAgIDxnIGlkPSJBc3NldF8ybG9nbyIgZGF0YS1uYW1lPSJBc3NldCAybG9nbyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTcwOC4zOTUgMjQxLjUyNSkiPgogICAgICA8ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTY1IC0xNCkiPgogICAgICAgIDxwYXRoIGlkPSJQYXRoXzkyMSIgZGF0YS1uYW1lPSJQYXRoIDkyMSIgZD0iTTEuOCw2Ny4xSDBWNDMuMTIyYTUuNDM0LDUuNDM0LDAsMCwxLDQuOTIyLTUuNTU4Yy45NTEtLjE0LDEzLjM2LTIuNTExLDI0LjY1NS00LjY4M2wuOS0uMTcxLjMzOCwxLjc3MS0uOS4xNjdjLTUuNDksMS4wNTktMjMuNSw0LjUtMjQuNzM3LDQuN0EzLjczLDMuNzMsMCwwLDAsMS44LDQzLjEyMloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgLTE3Ljk2NikiIGZpbGw9IiNkZDUxMDMiLz4KICAgICAgICA8cGF0aCBpZD0iUGF0aF85MjIiIGRhdGEtbmFtZT0iUGF0aCA5MjIiIGQ9Ik0xMTguNTgzLDIwMi43NDVsLS4zNDMtMS43NzEuOS0uMTcxYzEwLjcxLTIuMDc4LDQ2LjA3LTguOTExLDQ5LjE2Ny05LjMzNSwxLjI1My0uMTgsNy41MTgtMS40MzMsNy41MTgtOC40MzhWMTM2aDEuOHY0Ny4wM2MwLDYuNTg1LTQuODg2LDkuNjIzLTkuMDY5LDEwLjIxOC0zLjA1Ni40NTEtMzguMzU4LDcuMjQ4LTQ5LjEsOS4zMjZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNjQuOTQ1IC03NC43KSIgZmlsbD0iIzAwODJjMyIvPgogICAgICAgIDxwYXRoIGlkPSJQYXRoXzkyMyIgZGF0YS1uYW1lPSJQYXRoIDkyMyIgZD0iTTAsMjIzdjM2Ljk2TDE5Ljk5LDI1NiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAtMTIyLjQ4NSkiIGZpbGw9IiNkZDUxMDMiLz4KICAgICAgICA8cGF0aCBpZD0iUGF0aF85MjQiIGRhdGEtbmFtZT0iUGF0aCA5MjQiIGQ9Ik0yMjUuNjQsMzYuOTYxVjBMMjA1LjY1LDMuOTYyIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMTEyLjk1NikiIGZpbGw9IiMwMDgyYzMiLz4KICAgICAgPC9nPgogICAgPC9nPgogICAgPGcgaWQ9IkFzc2V0XzExc2VjdXJlIiBkYXRhLW5hbWU9IkFzc2V0IDExc2VjdXJlIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMTgzLjc3MSAyNzAuMzc0KSI+CiAgICAgIDxnIGlkPSJDYWxxdWVfMiIgZGF0YS1uYW1lPSJDYWxxdWUgMiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCkiPgogICAgICAgIDxwYXRoIGlkPSJQYXRoXzUwNSIgZGF0YS1uYW1lPSJQYXRoIDUwNSIgZD0iTTMuOTgsMjEuMjA2YTEuMSwxLjEsMCwwLDEtMS4xLTEuMVY5LjU0N2E5LjU0Nyw5LjU0NywwLDEsMSwxOS4wOTQsMHY3LjM5MWExLjEyMiwxLjEyMiwwLDAsMS0xLjEsMS4xLDEuMSwxLjEsMCwwLDEtMS4xLTEuMVY5LjU0N2E3LjM0Nyw3LjM0NywwLDEsMC0xNC42OTUsMFYyMC4xMDZBMS4xLDEuMSwwLDAsMSwzLjk4LDIxLjIwNloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMuNDU2IDApIiBmaWxsPSIjZWM2NjA3IiBzdHJva2U9IiNlYzY2MDciIHN0cm9rZS13aWR0aD0iMSIvPgogICAgICAgIDxwYXRoIGlkPSJQYXRoXzUwNCIgZGF0YS1uYW1lPSJQYXRoIDUwNCIgZD0iTTEuMSwzOC45NDJBMS4xNDQsMS4xNDQsMCwwLDEsLjQsMzguN2ExLjE2NiwxLjE2NiwwLDAsMS0uNC0uODU4di0yNC4yYTEuMSwxLjEsMCwwLDEsLjktMS4xTDMwLjQ2Nyw3LjIyMWExLjEyMiwxLjEyMiwwLDAsMSwuOS4yMiwxLjE0NCwxLjE0NCwwLDAsMSwuMzc0Ljg1OFYzMi41YTEuMSwxLjEsMCwwLDEtLjksMS4xTDEuMywzOC45MlpNMi4yLDE0LjQ4djIybDI3LjM0NC00Ljg4NFY5LjZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDguNjQpIiBmaWxsPSIjMDA4MmMzIiBzdHJva2U9IiMwMDc5YmEiIHN0cm9rZS13aWR0aD0iMSIvPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K"
                  alt="User Login"
                />
              </div>
            </div>
          </div>
        </Col>
        <Col xs={24} lg={12} sm={24} md={12}>
          <Form layout="vertical">
            <Form.Item
              label={<b>User Name</b>}
              name="username"
              rules={[{ required: true, message: "Please input your username!" }]}
              className="mt-2"
            >
              <Input
                value={username}
                onChange={e => {
                  setUserName(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item
              label={<b>Password</b>}
              name="password"
              rules={[{ required: true, message: "Please input your password!" }]}
              className="mt-2"
            >
              <Input.Password
                onChange={e => {
                  setPassword(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button key="1" type="primary" className="float-right" htmlType="submit" onClick={() => handleSubmit()}>
                Login
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Login;
