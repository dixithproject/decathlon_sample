import React, { lazy } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

/**
 * Application Routes Configuration
 * @param props 
 */
const PublicRoutes = () => {
  return (
    <BrowserRouter>
      <>
        <Switch>
          <Route
            path="/:unique_name"
            component={lazy(() => import("../app"))}
          />
        </Switch>
      </>
    </BrowserRouter>
  );
};

export default PublicRoutes;
