import React, { Component, lazy, Suspense } from "react";
import { Redirect, Route } from "react-router-dom";
import Loader from "../uiComponents/loader";

const Routes = [
  {
    path: ":unique_name",
    component: lazy(() => import("../pages/home"))
  },
  {
    path: ":unique_name/login",
    component: lazy(() => import("../pages/login"))
  },
  {
    path: ":unique_name/checkout",
    component: lazy(() => import("../pages/cart"))
  },
  {
    path: ":unique_name/product/:product_id/details",
    component: lazy(() => import("../pages/product/details"))
  }
];

class AppRouter extends Component {
  render() {
    return (
      <Suspense fallback={<Loader />}>
        <>
          {Routes.map(singleRoute => {
            const { name, path, exact, ...otherProps } = singleRoute;

            if (path === undefined) {
              return <Redirect to={{ pathname: "/404" }} />;
            }

            return <Route exact={exact === false ? false : true} key={path} path={`/${path}`} {...otherProps} />;
          })}
        </>
      </Suspense>
    );
  }
}

export default AppRouter;
