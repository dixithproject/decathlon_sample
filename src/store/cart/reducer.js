import { cloneDeep, filter, get, map } from "lodash";
import { getCookie, setCookie } from "../../helper/cookieHelper";
import cartActions from "./actions";

const initState = {
  cart: getCookie("cart_items") ? getCookie("cart_items") : [],
  finalCartItems: null,
  cartItemsCount: 0,
};

export default function cartReducer(state = initState, action) {
  switch (action.type) {
    case cartActions.ADD_TO_CART:
      let addData = cloneDeep(get(state, "cart", []));
      let updatedState = [];
      if (addData.length > 0) {
        map(addData, function (o) {
          if (o.id === get(action, "data.id")) {
            o["qty"] = get(o, "qty", 0) + 1;
            updatedState.push(o);
          } else {
            updatedState = [...addData, action.data];
          }
        });
      } else {
        updatedState = [...addData, action.data];
      }
      
      setCookie("cart_items", updatedState);
      return {
        ...state,
        cart: updatedState,
        cartItemsCount: updatedState.length,
      };
    case cartActions.REMOVE_FROM_CART:
      const cartOptions = filter(cloneDeep(get(state, "cart", [])), function (o) {
        return o.id !== action.data;
      });

      let optionsToBeRemoved = filter(cloneDeep(get(state, "cart", [])), function (o) {
        return o.id === action.data;
      });

      let computedCart = [];

      if (optionsToBeRemoved) {
        optionsToBeRemoved.pop();
        computedCart = [...cartOptions, ...optionsToBeRemoved];
      }
      setCookie("cart_items", computedCart);
      return {
        ...state,
        cartItemsCount: optionsToBeRemoved.length > 0 ? state.cartItemsCount - 1 : state.cartItemsCount,
        cart: computedCart,
      };
    default:
      return state;
  }
}
