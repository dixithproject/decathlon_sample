
const cartActions = {
  ADD_TO_CART: "ADD_TO_CART",
  REMOVE_FROM_CART: "REMOVE_FROM_CART",
  addToCart: (data) => ({
    type: cartActions.ADD_TO_CART,
    data,
  }),
  removeFromCart: (data) => ({
    type: cartActions.REMOVE_FROM_CART,
    data,
  })
};

export default cartActions;