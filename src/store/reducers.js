import Auth from "./auth/reducer";
import Cart from "./cart/reducer";
import User from "./user/reducer";

export default { Auth, Cart, User };
