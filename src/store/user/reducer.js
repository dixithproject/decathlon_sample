import userStoreActions from "./actions";

const initState = {
  store_info: []
};

export default function userStoreReducer(state = initState, action) {
  switch (action.type) {
    case userStoreActions.SAVE_STORE_INFO:
      return {
        ...state,
        store_info: action.data
      };
    default:
      return state;
  }
}
