
const userStoreActions = {
  SAVE_STORE_INFO: "SAVE_STORE_INFO",
  saveStoreInfo: (data) => ({
    type: userStoreActions.SAVE_STORE_INFO,
    data,
  })
};

export default userStoreActions;