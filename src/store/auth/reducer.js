import authActions from "./actions";

const initState = {
  userDetails: null
};

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case authActions.SAVE_USER:
      return {
        ...state,
        userDetails: action.data
      };
    default:
      return state;
  }
}
