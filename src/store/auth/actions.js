
const authActions = {
  SAVE_USER: "SAVE_USER",
  saveUser: (data) => ({
    type: authActions.SAVE_USER,
    data,
  })
};

export default authActions;