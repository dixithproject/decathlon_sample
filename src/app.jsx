import { get } from "lodash";
import React, { Suspense } from "react";
import Layout from "./layout";
import AppRouter from "./routes/appRouter";
import UserActions from "./store/user/actions";
import Loader from "./uiComponents/loader";
const { saveStoreInfo } = UserActions;

const MainContent = props => {
  return (
    <div className="app">
      <Layout unique_name={get(props, "match.params.unique_name")} {...props}>
        <AppRouter />
      </Layout>
    </div>
  );
};

/**
 * Application Entry Component
 * @param props
 */
function App(props) {
  const { match } = props;

  return (
    <Suspense fallback={<Loader />}>
      <MainContent match={match} />
    </Suspense>
  );
}

export default App;
