import React, { Suspense } from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import Router from "./routes";
import { history, store } from "./store/rootStore";
import Loader from "./uiComponents/loader";

const Entry = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Suspense fallback={<Loader />}>
                    <Router history={history} />
                </Suspense>
            </BrowserRouter>
        </Provider>
    );
};

export default Entry;