export const productList = [
  {
    id: 1,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp1852732%2F1a5328359fa7bc34c6702155128f8aee%2Fp1852732.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "Men's Trekking Padded Jacket - MT50 0°C",
    item_price: 999,
    mrp_price: 1699,
    calculated_price: 1699,
    rating: 4.5
  },
  {
    id: 2,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp1420005%2F4067736fa30c86fd7c12291607df5399%2Fp1420005.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "Men's Urban Walking Shoes PW 100 - grey",
    item_price: 599,
    mrp_price: 999,
    calculated_price: 999,
    rating: 4
  },
  {
    id: 3,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp424563%2F65eaabd17d874331ce1d119bc109c464%2Fp424563.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "Men Basic Fitness Tracksuit Jacket - Black",
    item_price: 799,
    mrp_price: 1699,
    calculated_price: 1699,
    rating: 3
  },
  {
    id: 4,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp2138547%2Fb652956f104d31bc27f0fbe8e62fbe45%2Fp2138547.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "Men Recycled Polyester Gym T-Shirt - Black Print",
    item_price: 499,
    mrp_price: 799,
    calculated_price: 1499,
    rating: 3
  },
  {
    id: 5,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp1705805%2F9bc321f7b4ef51b39e26ff25771d7d05%2Fp1705805.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "120 Women's Fitness Cardio Training Leggings - Burgundy",
    item_price: 499,
    mrp_price: 899,
    calculated_price: 1499,
    rating: 5
  },
  {
    id: 6,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp1948734%2Fbe2925a97ff09ec92c124bf683b880fe%2Fp1948734.jpg%26format%3Dauto&w=384&q=75",
    item_name: "Men's Hiking Fleece - MH20",
    item_price: 799,
    mrp_price: 1499,
    calculated_price: 1499,
    rating: 4
  },
  {
    id: 7,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp1759400%2F1d7fefe6901750c53c2e7e390eb421da%2Fp1759400.jpg%26format%3Dauto&w=384&q=75",
    item_name: "Men's Trekking Padded Jacket - MT50 0°C",
    item_price: 799,
    mrp_price: 1499,
    calculated_price: 1499,
    rating: 4.5
  },
  {
    id: 8,
    image_src:
      "https://www.decathlon.in/_next/image?url=https%3A%2F%2Fcontents.mediadecathlon.com%2Fp2017063%2Fe6a373ca9ea7e75022f5889876c939b8%2Fp2017063.jpg%3Fformat%3Dauto&w=384&q=75",
    item_name: "Men's Waterproof Hiking Overtrousers NH500",
    item_price: 699,
    mrp_price: 1499,
    calculated_price: 1199,
    rating: 4
  }
];
