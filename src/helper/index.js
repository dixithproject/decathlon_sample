import { round } from "lodash";

export function clearToken() {
  localStorage.removeItem("id_token");
}

export function setToken(value) {
  localStorage.setItem("id_token", value);
}

export function getToken() {
  return localStorage.getItem("id_token");
}

export const getSumArrayOfObjWithKey = (arr, key) => {
  let sumValue = arr.reduce((prev, cur) => {
    return prev + round(cur[key], 2);
  }, 0);
  return sumValue;
};

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function getScreenSize() {
  return window.innerWidth || document.body.clientWidth;
}
