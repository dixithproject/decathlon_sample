import { LoginOutlined, ShoppingCartOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, Button, message, PageHeader } from "antd";
import { get } from "lodash";
import React from "react";
import { Link } from "react-router-dom";
import { getToken } from "../helper";
import { getCookie } from "../helper/cookieHelper";
import "./index.scss";

const Layout = props => {
  const cartItemsList = getCookie("cart_items") ? getCookie("cart_items") : [];
  const { match, history } = props;

  return (
    <div className="site-page-header-ghost-wrapper">
      <PageHeader
        ghost={false}
        onBack={false}
        title={
          <Link to={`/${get(match, "params.unique_name")}`}>
            <img
              className="img-fluid mb-3"
              src="https://cdncontent.decathlon.in/_next/static/images/logo-93d12d8cff484ab736d2a39f15bf66d8.svg"
              alt="Decathlon"
            />
          </Link>
        }
        subTitle="All Sports"
        extra={[
          <Badge count={cartItemsList.length} offset={[-10, -5]} overflowCount={9}>
            <Link to={`/${get(match, "params.unique_name")}/checkout`}>
              <Button key="2" icon={<ShoppingCartOutlined />}>
                Cart
              </Button>
            </Link>
          </Badge>,
          getToken() ? (
            <Button key="1" type="primary" icon={<UserOutlined />} onClick={() => message.info("Coming Soon")}>
              Account
            </Button>
          ) : (
            <Link to={`/${get(match, "params.unique_name")}/login`}>
              <Button key="1" type="primary" icon={<LoginOutlined />}>
                Login
              </Button>
            </Link>
          )
        ]}
        footer={
          <div className="copyright_footer text-center mt-5">
            <p className="text-uppercase text-center mb-4 ourPurpose">
              <b>Our Purpose</b>
            </p>
            <p className="text-center mb-4">
              <span>To Sustainably </span>
              Make the Pleasure and Benefits of Sports Accessible to the Many
            </p>
            <div className="footer-logo mb-4">
              <img
                className="img-fluid"
                src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDA1LjM1OCIgaGVpZ2h0PSIyMTUuMDU2IiB2aWV3Qm94PSIwIDAgMTQwNS4zNTggMjE1LjA1NiI+PGRlZnM+PHN0eWxlPi5he29wYWNpdHk6MC4xMTM7fS5ie2ZpbGw6IzQyNDQ1Mzt9PC9zdHlsZT48L2RlZnM+PGcgY2xhc3M9ImEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgLTE4KSI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxOCkiPjxwYXRoIGNsYXNzPSJiIiBkPSJNNzcsMS43OUgxOTVWNDcuMTY2bC02NC45MjMuMDQ4djMzLjdsNTYuMjA5LS4wNDh2NDQuMzE3bC01Ni4yMDkuMXYzNy4zNkgxOTV2NDUuMjA4SDc3WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTA4LjM1NiAyLjUxOSkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTM2NC40LDQuMjEzcy02OC4yOTMsMTE2LjItNzQuOTM3LDEyNi42NjhjLTcuOCwxMi4zMjUtMjQuNzQ2LDMxLjgyMy01MC4zODMsMzEuNDg2LTQxLjc0MS0uNjI2LTU3LjQxMi0yOS40NjQtNTcuNDEyLTU1LjM2NmE1NS4zNjYsNTUuMzY2LDAsMCwxLDU0LjEzOC01NS4zNjZjMzQuNjY0LS43LDYwLjQ0NSwzMi41LDYwLjQ0NSwzMi41bDI3LjEwNS00My42NjdTMjkyLjY4OC0xLjE3OSwyMzYuMzExLjFjLTg0LjksMS44NzgtMTA5LjYsNzMuMDExLTEwOS42LDEwNS45MTgsMCw2OS40LDUzLjQ0LDEwNC4wNjQsOTcuMDU5LDEwOC4zMjVDMjg2LjYsMjIwLjQ1NCwzMjEsMTc5LjQxLDMyMSwxNzkuNDFzLS40MzMsMCwxLjcwOS0uMTY5YzEyLjY4Ni0uMTQ0LDQ2LjEyMi4xNjksNDYuMTIyLjE2OXYzMC44NjFoNTEuMDU3VjQuMjEzWm01LjQ2NCwxMzIuMDM2SDM0Mi41MTdsMjcuMzQ2LTQ2LjY3NloiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDE3OC4zMDggMC4wOTYpIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0yOTQuMzExLDIwOC4wMVY1MS4wMzZIMjUzLjFWMS43NkgzODguNTA2VjUxLjAzNkgzNDcuMzY3VjIwOC4wMVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM1Ni4xNjcgMi40NzcpIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik00MjEuNTIzLDEyOS4xaC01NC42djc4Ljg4NUgzMTMuOTJWMS43OGg1My4wMDdWODAuMDg3aDU0LjZWMS43OGg1My4wMzF2MjA2LjJINDIxLjUyM1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDQ0MS43NTQgMi41MDUpIi8+PHBhdGggY2xhc3M9ImIiIGQ9Ik0zODcuMzgsMjA3Ljk1NFYxLjhoNTIuNzQyVjE2Mi40MWg2MS44OXY0NS41NDVaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg1NDUuMTI4IDIuNTMzKSIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNNTI5LjE5MiwxNjMuNTQ2YzMwLjA0MiwwLDU0LjQ1MS0yMy44OCw1NC40NTEtNTcuNzczYTU0LjczNCw1NC43MzQsMCwxLDAtMTA5LjQ2OCwwYy4wMTIsMzIuNjY2LDI2LjE3OSw1Ny43NzMsNTUuMDE3LDU3Ljc3M20tNzUuOS0xMzMuMjE1QzQ3My45LDEwLjIwNyw0OTkuNjc5LDAsNTI5LjQ4LDBhMTA2LjMyNywxMDYuMzI3LDAsMCwxLDc0Ljc0NSwzMC42MiwxMDQuNCwxMDQuNCwwLDAsMSwzMi4zNTMsNzUuNTE0QTEwNi44OCwxMDYuODgsMCwwLDEsNTI5LjE5MiwyMTQuOTE2Yy0zMC45MDksMC01Ny4zNjQtMTEuMDQ5LTc3Ljk5NC0zMi43MTQtMTkuNjY3LTIwLjY1NC0yOS42NTctNDYuMjkxLTI5LjY1Ny03Ni45MTFhMTAzLjM2NywxMDMuMzY3LDAsMCwxLDMxLjc1MS03NC45NjEiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDU5My4xOTggMCkiLz48cGF0aCBjbGFzcz0iYiIgZD0iTTUwOC44OCwyMDcuOTY4VjEuNzloNDcuODA4TDYzNi4yNDcsMTE2LjNWMS43OWg1My4wMDdWMjA3Ljk2OGgtNDYuNkw1NjEuODM5LDkzLjEyVjIwNy45NjhaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg3MTYuMTA1IDIuNTE5KSIvPjxwYXRoIGNsYXNzPSJiIiBkPSJNMTM0LjE1NCwxMDQuMTY5YzAtMzcuMTE5LTIxLjA2My01OC4wMzgtNjAuMDM2LTU4LjAzOEg1Mi45NTlWMTYyLjY4OEg3NC4zODNjNDAuNTg2LDAsNTkuNzcxLTE3LjkzNCw1OS43NzEtNTguNTE5TTAsMjA3Ljk2OFYxLjc5SDc0LjExOGMzNC40NDcsMCw1OC41MTksNC4zMzMsODEuMSwyNC41NzgsMjAuNjc4LDE4LjU2LDMxLjY1NSw0NS41MiwzMS42NTUsNzkuNDM4LDAsMzEuMjk0LTEwLjY2NCw1Ny4xLTMxLjI5NCw3Ni41NS0yMy4xMzMsMjEuNDI0LTQzLjU3MSwyNS41MTYtNzQuMDcsMjUuNTE2WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAyLjUxOSkiLz48L2c+PC9nPjwvc3ZnPg=="
                alt="Decathlon"
                crossOrigin="anonymous"
              />
            </div>
            <p className="copyright text-center">© 2021 Decathlon Sports India Pvt Ltd. All rights reserved.</p>
          </div>
        }
      >
        {props.children}
      </PageHeader>
    </div>
  );
};

export default Layout;
