import "antd/dist/antd.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import ReactDOM from "react-dom";
import "./assets/styles/index.scss";
import Entry from "./entry";

ReactDOM.render(
  <Entry />,
  document.getElementById("root")
);
